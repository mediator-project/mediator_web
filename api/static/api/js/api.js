/**
 * Note : All the API url constants are defined in the api/api_js template
 */

/**
 * Class representation of the person api
 * @type {Person}
 */
let Person = class {

    /**
     * Create a new person.
     * Do not use directly to create a person, use the 'get' or 'create' static method for this
     * @param id Set to null for an unsaved person on the server
     * @param name
     * @param own_moral
     * @param own_media
     * @param authored
     */
    constructor(id, name, own_moral, own_media, authored) {
        this.id = id;
        this.name = name;
        this.own_moral = own_moral;
        this.own_media = own_media;
        this.authored = authored;
    }

    /**
     * Get the JSON string of the person object
     */
    json_str() {
        return JSON.stringify({
            id: this.id,
            name: this.name,
            own_moral: this.own_moral,
            own_media: this.own_media,
            authored: this.authored
        })
    }

    static from_json(json) {
        return new Person(json.id, json.name, json.own_moral, json.own_media, json.authored);
    }

    /**
     * Get the 'list' and 'create' api url for a person
     * @returns {string}
     */
    static url_list() {
        return api_url_person;
    }

    /**
     * Get the 'get', 'update' api url for a person
     * @param id person unique id
     * @returns {string}
     */
    static url_get(id) {
        return api_url_person + id;
    }

    /**
     * List the persons
     * @param search Person name to search
     */
    static list(search=null) {
        var url = Person.url_list();

        if (search != null) {
            url = url + "?search=" + search;
        }

        return fetch(url, {
            method: 'GET'
        }).then(resp => resp.json())
            .then(data => {
                var persons = [];

                for (var d in data){
                    var d = data[d];
                    persons.push(Person.from_json(d));
                }

                return persons;
            })
            .catch(error => console.warn(error));
    }

    /**
     * Get a person from it's unique id
     * @param id
     */
    static get(id){
        return fetch(Person.url_get(id), {
            method: 'GET'
        }).then((resp) => resp.json())
            .then((data) => {
                return Person.from_json(data);
            })
            .catch(error => console.warn(error));
    }

    /**
     * Save the person on the API. The the person does not exist (id is null), then the person will be created
     * and attributed an id
     */
    save() {
        if (this.id == null) {
            return this._create();
        } else {
            return this._update();
        }
    }

    /**
     * Create an unexisting person on the server
     * @private
     */
    _create() {
        return fetch(Person.url_list(), {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: this.json_str()
        }).then((resp) => resp.json())
            .then((data) => {
                //this = Person.from_json(data);
                return this;
            });
    }

    /**
     * Update an existing person on the server
     * @private
     */
    _update() {
        return fetch(Person.url_get(this.id), {
            methood: 'PUT',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: this.json_str()
        }).then((resp) => resp.json())
            .then((data) => {
                //this = Person.from_json(data);
                return this;
            })
    }

};

let Moral = class {
    constructor(id, name, own_moral, own_media, owned_by_person, owned_by_moral) {
        this.id = id;
        this.name = name;
        this.own_moral = own_moral;
        this.own_media = own_media;
        this.owned_by_person = owned_by_person;
        this.owned_by_moral = owned_by_moral;
    }

    json_str() {
        return JSON.stringify({
            id: this.id,
            name: this.name,
            own_moral: this.own_moral,
            own_media: this.own_media,
            owned_by_person: this.owned_by_person,
            owned_by_moral: this.owned_by_moral
        })
    }

    static from_json(json) {
        return new Moral(json.id, json.name, json.own_moral, json.own_media, json.owned_by_person, json.owned_by_moral);
    }

    static url_list() {
        return api_url_moral;
    }

    static url_get(id) {
        return Moral.url_list() + id;
    }

    /**
     * List the moral entities
     * @param search Search by entity name
     */
    static list(search=null) {
        var url = Moral.url_list();

        if (search != null) {
            url += '?search=' + search;
        }

        return fetch(url, {
            method: 'GET'
        }).then(resp => resp.json())
            .then(data => {
                var morals = [];

                for (var d in data){
                    var d = data[d];
                    morals.push(Moral.from_json(d));
                }

                return morals;
            })
            .catch(error => console.warn(error));
    }
};

/**
 *
 * @type {Media}
 */
let Media = class {
    constructor(id, name, url, owned_by_person, owned_by_moral) {
        this.id = id;
        this.name = name;
        this.url = url;
        this.owned_by_person = owned_by_person;
        this.owned_by_moral = owned_by_moral;
    }

    /**
     * Get a JSON string
     */
    json_str() {
        return JSON.stringify({
            id: this.id,
            name: this.name,
            url: this.url,
            owned_by_person: this.owned_by_person,
            owned_by_moral: this.owned_by_moral
        })
    }

    /**
     * Create a new media from JSON object
     * @param json
     */
    static from_json(json) {
        return new Media(json.id, json.name, json.url, json.owned_by_person, json.owned_by_moral);
    }

    static url_list() {
        return api_url_media;
    }

    static url_get(id) {
        return Media.url_list() + id;
    }

    /**
     * List the medias
     * @param search Search by medias name
     */
    static list(search=null) {
        var url = Media.url_list();

        if (search != null) {
            url += '?search=' + search;
        }

        return fetch(url, {
            method: 'GET'
        }).then(resp => resp.json())
            .then(data => {
                var medias = [];

                for (var d in data){
                    var d = data[d];
                    medias.push(Media.from_json(d));
                }

                return medias;
            })
            .catch(error => console.warn(error));
    }
};

let Content = class {
    constructor(id, media, title, description, url, paywalled, authors) {
        this.id;
        this.media = media;
        this.title = title;
        this.description = description;
        this.url = url;
        this.paywalled = paywalled;
        this.authors = authors;
    }

    json_str() {
        var authors = [];
        for (var author in this.authors){
            authors.push(author.id);
        }

        return JSON.stringify({
            id: this.id,
            media: this.media.id,
            title: this.title,
            description: this.description,
            url: this.url,
            paywalled: this.paywalled,
            authors: authors
        });
    }

    static from_json(json) {
        return new Content(json.id, json.media, json.title, json.description, json.url, json.paywalled, json.authors);
    }

    static url_list() {
        return api_url_content;
    }

    static url_get(id) {
        return Content.url_list() + id;
    }

    /**
     * List the contents
     * @param search search by content title
     */
    static list(search) {
        var url = Content.url_list();

        if (search != null) {
            url += '?search=' + search;
        }

        return fetch(url, {
            method: 'GET'
        }).then(resp => resp.json())
            .then(data => {
                var contents = [];

                for (var d in data){
                    var d = data[d];
                    contents.push(Content.from_json(d));
                }

                return contents;
            })
            .catch(error => console.warn(error));
    }
};