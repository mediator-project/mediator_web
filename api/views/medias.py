from rest_framework import viewsets
from rest_framework.filters import SearchFilter

from api.models import *
from api.serializers import *


class MediaViewSet(viewsets.ModelViewSet):
    """
    Media read only view
    """
    queryset = Media.objects.all()
    serializer_class = MediaROSerializer
    filter_backends = [SearchFilter]
    search_fields = ['name']

    def get_serializer_class(self):
        if self.action in ['list', 'retrieve']:
            return MediaROSerializer
        else:
            return MediaAddSerializer