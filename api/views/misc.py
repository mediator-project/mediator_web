from django.views.generic import TemplateView


class ApiJsTestView(TemplateView):
    template_name = 'api/test_js_api.html'