from rest_framework import viewsets
from rest_framework.filters import SearchFilter

from api.models import *
from api.serializers import *


class ContentViewSet(viewsets.ModelViewSet):
    """
    Content read only view set
    """
    queryset = Content.objects.all()
    serializer_class = ContentROSerializer
    filter_backends = [SearchFilter]
    search_fields = ['name']

    def get_serializer_class(self):
        if self.action in ['list', 'retrieve']:
            return ContentROSerializer
        else:
            return ContentAddSerializer