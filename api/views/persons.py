from rest_framework import viewsets
from rest_framework.filters import SearchFilter
from rest_framework.mixins import CreateModelMixin
from rest_framework.response import Response

from api.models import *
from api.serializers import *


class PersonViewSet(viewsets.ModelViewSet):
    """
    Person api.
    """
    queryset = Person.objects.all()
    filter_backends = [SearchFilter]
    search_fields = ['name']

    def get_serializer_class(self):
        if self.action in ['list', 'retrieve']:
            return PersonROSerializer
        else:
            return PersonAddSerializer


class STViewSet(viewsets.ModelViewSet):
    """
    This is for test only : to remove ASAP.
    """
    queryset = SpatioTemporalLocation.objects.all()
    serializer_class = SpatioTemporalLocationSerializer