from rest_framework import viewsets
from rest_framework.filters import SearchFilter

from api.models import *
from api.serializers import *


class MoralEntityViewSet(viewsets.ModelViewSet):
    """
    Moral entity api
    """
    queryset = MoralEntity.objects.all()
    serializer_class = MoralEntityROSerializer
    filter_backends = [SearchFilter]
    search_fields = ['name']

    def get_serializer_class(self):
        if self.action in ['list', 'retrieve']:
            return MoralEntityROSerializer
        else:
            return MoralEntityAddSerializer