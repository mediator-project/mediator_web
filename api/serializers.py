from rest_framework import serializers

from api.models import *


class PersonROSerializer(serializers.ModelSerializer):
    """
    Person read only serializer. Also serialize the related fields.
    """
    class Meta:
        model = Person
        fields = ['id', 'name', 'own_moral', 'own_media', 'authored']


class PersonShortRoSerializer(serializers.ModelSerializer):
    """
    Light person read only serializer. Get only the id and the name of the person.
    This serializer is destined to be embed as nested serializer.
    """
    class Meta:
        model = Person
        fields = ['id', 'name']


class PersonAddSerializer(serializers.ModelSerializer):
    """
    Person serializer without the ID field.
    """
    class Meta:
        model = Person
        fields = ['name', 'own_moral', 'own_media']


class MoralEntityROSerializer(serializers.ModelSerializer):
    """
    Moral entity read only serializer. Also serialize the related fields.
    """
    class Meta:
        model = MoralEntity
        fields = ['id', 'name', 'own_moral', 'own_media', 'owned_by_moral', 'owned_by_person']


class MoralEntityAddSerializer(serializers.ModelSerializer):
    """
    Moral entity serializer without the ID field.
    """
    class Meta:
        model = MoralEntity
        fields = ['name', 'own_moral', 'own_media', 'owned_by_moral', 'owned_by_person']


class MediaROSerializer(serializers.ModelSerializer):
    """
    Media read only serializer. Also serialize the related fields.
    """
    class Meta:
        model = Media
        fields = ['id', 'name', 'url', 'owned_by_moral', 'owned_by_person']


class MediaShortROSerializer(serializers.ModelSerializer):
    """
    Short media read only serializer. Serialize only the id and the name of the media.
    This serializer is destined to be embed as a nested serializer in other serializers
    """
    class Meta:
        model = Media
        fields = ['id', 'name']


class MediaAddSerializer(serializers.ModelSerializer):
    """
    Media serializer without the ID field.
    """
    class Meta:
        model = Media
        fields = ['name', 'url', 'owned_by_moral', 'owned_by_person']


class ContentROSerializer(serializers.ModelSerializer):
    """
    Content read only serializer. Also serialize the related fields.
    """
    media = MediaShortROSerializer(read_only=True)
    authors = PersonShortRoSerializer(many=True, read_only=True)

    class Meta:
        model = Content
        fields = ['id', 'media', 'title', 'description', 'url', 'paywalled', 'authors']


class ContentAddSerializer(serializers.ModelSerializer):
    """
    Content serializer without the ID field.
    """
    class Meta:
        model = Content
        fields = ['media', 'title', 'description', 'url', 'paywalled', 'authors']


class SpatioTemporalLocationSerializer(serializers.ModelSerializer):
    """
    Serialize a spatio-temporal location
    """
    class Meta:
        model = SpatioTemporalLocation
        fields = ['content', 'start', 'end', 'point']