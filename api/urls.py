from django.urls import path
from rest_framework import routers

from api.views.misc import ApiJsTestView
from api.views.persons import *
from api.views.morals import *
from api.views.medias import *
from api.views.contents import *


router = routers.DefaultRouter()

router.register(r'persons', PersonViewSet, basename='person')
router.register(r'morals', MoralEntityViewSet, basename='moral')
router.register(r'medias', MediaViewSet, basename='media')
router.register(r'contents', ContentViewSet, basename='content')

router.register(r'stl', STViewSet)

urlpatterns = [
    path(r'test_js', ApiJsTestView.as_view())
] + router.urls