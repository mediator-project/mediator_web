from django.db import models
from django.contrib.gis.db import models


class Person(models.Model):
    """
    A physical person
    """
    # Full name
    name = models.CharField(max_length=1024)
    own_moral = models.ManyToManyField("MoralEntity", related_name='owned_by_person')


class MoralEntity(models.Model):
    """
    A moral entity (company, association, ...) - not a person -
    """
    # Full name
    name = models.CharField(max_length=1024)
    own_moral = models.ManyToManyField("MoralEntity", related_name="owned_by_moral")


class Media(models.Model):
    """
    A diffusion canal (website, youtube channel, twitter, facebook, ...)
    """
    name = models.CharField(max_length=1024)
    url = models.URLField()

    # Should be owned by a moral or a person. Not both, even if for now it is allowed
    owned_by_moral = models.ForeignKey(MoralEntity, related_name='own_media', null=True, on_delete=models.CASCADE)
    owned_by_person = models.ForeignKey(Person, related_name='own_media', null=True, on_delete=models.CASCADE)


class Content(models.Model):
    """
    Content produced by a media
    """
    media = models.ForeignKey(Media, related_name='contents', on_delete=models.CASCADE)
    title = models.CharField(max_length=1024)
    description = models.CharField(max_length=4096)
    url = models.URLField()
    paywalled = models.BooleanField(null=True)
    authors = models.ManyToManyField(Person, related_name="authored")


class SpatioTemporalLocation(models.Model):
    """
    Spatio-temporal location for a content
    """
    content = models.ForeignKey(Content, related_name='locations', on_delete=models.CASCADE)
    start = models.DateTimeField()
    end = models.DateTimeField()
    point = models.PointField()
