from django.http import HttpResponseRedirect
from django.views.generic import ListView, DetailView, FormView
from rest_framework.reverse import reverse_lazy

from api.models import Media
from webapp.forms import NewMediaForm


class MediaListView(ListView):
    """
    Browse a list of medias
    """
    template_name = 'webapp/media/medias.html'
    model = Media
    paginate_by = 100


class MediaView(DetailView):
    queryset = Media.objects.all()
    template_name = 'webapp/media/media.html'


class AddMediaView(FormView):
    template_name = 'webapp/media/media_add.html'
    form_class = NewMediaForm

    def form_valid(self, form):
        media = form.save()
        return HttpResponseRedirect(reverse_lazy('medias-get', args=[media.id]))