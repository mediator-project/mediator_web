from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.views import View
from django.views.generic import ListView, DetailView, FormView
from rest_framework.reverse import reverse_lazy

from api.models import Person
from webapp.forms import MoralOwnershipForm, MediaOwnershipForm, NewPersonForm


class PersonListView(ListView):
    """
    Browse a list of persons
    """
    template_name = 'webapp/person/persons.html'
    model = Person
    paginate_by = 100


class PersonView(DetailView):
    queryset = Person.objects.all()
    template_name = "webapp/person/person.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form_own_moral'] = MoralOwnershipForm
        context['form_own_media'] = MediaOwnershipForm
        return context


class AddPersonView(FormView):
    template_name = 'webapp/person/person_add.html'
    form_class = NewPersonForm

    def form_valid(self, form):
        person = form.save()
        return HttpResponseRedirect(reverse_lazy('persons-get', args=[person.id]))


class PersonOwnMediaView(View):
    def post(self, request, pk):
        person = get_object_or_404(Person, pk=pk)

        form = MediaOwnershipForm(data=request.POST)

        if form.is_valid():
            media = form.cleaned_data['media']
            media.owned_by_person = person
            media.save()
            return HttpResponseRedirect(reverse_lazy('persons-get', args=[person.id]))
        # TODO handle errors