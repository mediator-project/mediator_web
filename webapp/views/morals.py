from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.views import View
from django.views.generic import FormView, DetailView, ListView
from rest_framework.reverse import reverse_lazy

from api.models import MoralEntity
from webapp.forms import NewMoralForm, MediaOwnershipForm, MoralOwnershipForm, PersonOwnershipForm


class MoralEntityListView(ListView):
    """
    Browse a list of moral entities
    """
    template_name = 'webapp/moral/morals.html'
    model = MoralEntity
    paginate_by = 100


class MoralEntityView(DetailView):
    queryset = MoralEntity.objects.all()
    template_name = 'webapp/moral/moral.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form_own_moral'] = MoralOwnershipForm
        context['form_own_media'] = MediaOwnershipForm
        context['form_owned_by_moral'] = MoralOwnershipForm
        context['form_owned_by_person'] = PersonOwnershipForm
        return context


class MoralOwnMediaView(View):
    def post(self, request, pk):
        owner = get_object_or_404(MoralEntity, pk=pk)
        form = MediaOwnershipForm(data=request.POST)

        if form.is_valid():
            media = form.cleaned_data['media']
            media.owned_by_moral = owner
            media.save()
            return HttpResponseRedirect(reverse_lazy('morals-get', args=[owner.id]))
        # TODO handle errors


class AddMoralEntityView(FormView):
    template_name = 'webapp/moral/moral_add.html'
    form_class = NewMoralForm

    def form_valid(self, form):
        moral = form.save()
        return HttpResponseRedirect(reverse_lazy('morals-get', args=[moral.id]))