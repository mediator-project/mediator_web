from django.http import HttpResponseRedirect
from django.views.generic import ListView, DetailView, FormView
from rest_framework.reverse import reverse_lazy

from api.models import Content
from webapp.forms import NewContentForm


class ContentListView(ListView):
    """
    Browse a list of contents
    """
    template_name = 'webapp/content/contents.html'
    model = Content
    paginate_by = 100


class ContentView(DetailView):
    queryset = Content.objects.all()
    template_name = 'webapp/content/content.html'


class AddContentView(FormView):
    template_name = 'webapp/content/content_add.html'
    form_class = NewContentForm

    def form_valid(self, form):
        content = form.save()
        return HttpResponseRedirect(reverse_lazy('contents-get', args=[content.id]))