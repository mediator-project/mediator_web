from django import forms

from api.models import Person, MoralEntity, Media, Content


class NewPersonForm(forms.ModelForm):
    class Meta:
        model = Person
        fields = ['name']


class NewMoralForm(forms.ModelForm):
    class Meta:
        model = MoralEntity
        fields = ['name']


class NewMediaForm(forms.ModelForm):
    class Meta:
        model = Media
        fields = ['name', 'url']


class NewContentForm(forms.ModelForm):
    class Meta:
        model = Content
        fields = ['media', 'title', 'description', 'url', 'paywalled']


class MoralOwnershipForm(forms.Form):
    moral = forms.ModelChoiceField(queryset=MoralEntity.objects.all())


class PersonOwnershipForm(forms.Form):
    person = forms.ModelChoiceField(queryset=Person.objects.all())


class MediaOwnershipForm(forms.Form):
    media = forms.ModelChoiceField(queryset=Media.objects.all())
