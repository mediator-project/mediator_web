from django.urls import path

from webapp.views.persons import *
from webapp.views.morals import *
from webapp.views.medias import *
from webapp.views.contents import *

urlpatterns = [
    path('persons', PersonListView.as_view(), name='persons-list'),
    path('person', AddPersonView.as_view(), name='persons-add'),
    path('person/<int:pk>', PersonView.as_view(), name='persons-get'),
    path('person/<int:pk>/media', PersonOwnMediaView.as_view(), name='persons-media'),

    path('morals', MoralEntityListView.as_view(), name='morals-list'),
    path('moral', AddMoralEntityView.as_view(), name='morals-add'),
    path('moral/<int:pk>', MoralEntityView.as_view(), name='morals-get'),
    path('moral/<int:pk>/own/media', MoralOwnMediaView.as_view(), name='morals-own-media'),

    path('medias', MediaListView.as_view(), name='medias-list'),
    path('media', AddMediaView.as_view(), name='medias-add'),
    path('media/<int:pk>', MediaView.as_view(), name='medias-get'),

    path('contents', ContentListView.as_view(), name='contents-list'),
    path('content', AddContentView.as_view(), name='contents-add'),
    path('content/<int:pk>', ContentView.as_view(), name='contents-get')
]